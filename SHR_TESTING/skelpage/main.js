var basepath = "cgi/"

var CGI = "cgi/immoplus.cgi"

var placeholderimg = "Assets/placeholder.png"

require([
	'app',
	'Application/Router',
	'Application/menu/menu',
	'Application/api/immolog',
	'Application/api/ProjektListeManager',
	'Application/api/themes',
	'Application/footer/footer',
	'Application/directives/euro_neu/euro_neu',
	'Application/directives/panel/panel',
	'Application/directives/foldpanel/foldpanel',
	'Application/directives/labledrow/labledrow',
	'Application/directives/debug/debug',
	'Application/directives/bjoernstable/bjoernstable',
	'Application/directives/Kaufpreisraten/Kaufpreisraten',
	'Application/directives/showerrors/showerrors',
	'Application/directives/number/number',
	'Application/directives/textarealinewrap/textarealinewrap',
	'Application/api/errorreporter'
], function(app) {

	app.filter('filterBy', function() {
		return function(array, query) {

			if (!array | !query) return array;

			var parts = query && query.trim().split(/\s+/),
				keys = Object.keys(array[0]);

			if (!parts || !parts.length) return array;

			return array.filter(function(obj) {
				return parts.every(function(part) {
					return keys.some(function(key) {
						return String(obj[key]).toLowerCase().indexOf(part.toLowerCase()) > -1;
					});
				});
			});
		};
	});

	app.directive('keyTrap', function() {
		return function(scope, elem) {
			elem.bind('keydown', function(event) {
				scope.$broadcast('keydown', {
					code: event.keyCode
				});
			});
		};
	});

	//We already have a limitTo filter built-in to angular,
	//let's make a startFrom filter
	app.filter('startFrom', function() {
		return function(input, start) {
			if(!input) return [];
			start = +start; //parse to int
			return input.slice(start);
		};
	});


	angular.bootstrap(document.getElementsByTagName("body")[0], ['ImmoPlus']);
});
