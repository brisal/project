window.isMobile = function () {
	return (screen.width < 500 ||
		navigator.userAgent.match(/Android/i) ||
		navigator.userAgent.match(/webOS/i) ||
		navigator.userAgent.match(/iPhone/i) ||
		navigator.userAgent.match(/iPod/i));
	
};

define(
    [
    ]
    , function () {
        return angular.module('ImmoPlus', ['ngRoute', 'ui.bootstrap', 'ngAnimate','vcRecaptcha', 'utils.autofocus']);

    }
);





