function renderHead(){
    return '<!doctype html> '+
        '<html lang="en"> '+
        '<head> '+
        '    <style> '+
        '    body { '+
        '        width: 8.5in; '+
        '        margin: 0in .1875in; '+
        '        } '+
        '    .label{ '+
        '        width: 2.025in; '+
        '        height: .875in; '+
        '        padding: .125in .3in 0; '+
        '        margin-right: .125in; '+
        '        float: left; ' +
        '        text-align: center; '+
        '        overflow: hidden; '+
        '        outline: 1px dotted; '+
        '    } '+
        '    .page-break  { '+
        '        clear: left; '+
        '        display:block; '+
        '        page-break-after:always; '+
        '    } '+
        '    </style> '+
        '</head> '+
        '<body> ';
}
function renderFoot(){
    return '<div class="page-break"></div>'+
        '</body>'+
        '</html>';
}

function renderElement(content){
    return '<div class="label">'+content+'</div>';
}

function Avery5160 (LabelContents /*maximum of 30 strings to print on the labels*/ ) {
    return renderHead() + LabelContents.map(function(el){
        return renderElement(el);
    }).join("\n") + renderFoot();
}