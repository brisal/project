# http server

$_DOCROOT=$PWD
function getDocRoot {
    return "$_DOCROOT\http_docs"
}

function setup($endpointAddr){
    $http = [System.Net.HttpListener]::new();
    $http.Prefixes.Add($endpointAddr);
    $http.start();
    return $http;
}

function log(){
    write-output $args
    $args >> log.txt
}

class HTTPDFileHandler {
    $contentType = "text/plain";
    $handlerName = "Plain";
    [string]render($path, $context, $postcontent){
        return get-content -raw -path $path
    }
}

class HTTPDNotFoundHandler {
    $contentType = "text/plain";
    $handlerName = "Plain";
    [string]render($path, $context, $postcontent){
        $context.Response.StatusCode = 404;
        return "";
    }
}
class HTTPDPwshHandler {
    $contentType = "text/plain";
    $handlerName = "Powershell";
    [string] render($path, $context, $postcontent){
        log "evaluate $path $postcontent"
        $script = get-content -raw -path $path
        return Invoke-Expression $script
    }
}

function getExtension($path){
    if(-not ( test-path -path $path ))  { return ""; }
    $ext = (Get-childitem -Path $path).extension;
    return $ext;
}

function getMimeType($path){
    $ret = [HTTPDFileHandler]::new();

    $ext = getExtension($path)
    switch ($ext)  {
        ".js" { 
            $ret.contentType = "application/javascript"
        }
        ".html" {
            $ret.contentType = "text/html"
        }
        ".css" {
            $ret.contentType = "text/css" 
        }
        ".jpeg" {
            $ret.contentType = "image/jpeg"
        }
        ".png" {
            $ret.contentType = "image/png"
        } 
        ".svg" {
            $ret.contentType = "image/svg+xml"
        }
        ".ps1" {
            $ret = [HTTPDPwshHandler]::new();
        }
        Default {
            $ret = [HTTPDNotFoundHandler]::new();
        }
    }

    return $ret;
}

function resolveRequestPath($path){
    $documentRoot = getDocRoot;
    return $documentRoot + "\" + $path.split("?")[0];
}

function windowsifyUrl($urlpath){
    $urlpath -replace '/', '\';
}

function resolveContent{
    param ( $path, $context, $handler, $postcontent)
    return $handler.render($path, $context, $postcontent)
}

function handleClient($context){
    $requestedPath =  resolveRequestPath(windowsifyUrl($context.request.RawUrl));
    $handler = getMimeType($requestedPath);
    $contentType = $handler.contentType;
    $context.Response.ContentType = $contentType;

    $method = $context.request.HttpMethod;
    log "$method [$requestedPath] ($contentType)"
    if ($method -eq "Get") {
        $message = resolveContent -path $requestedPath -context $context -handler $handler
    } else {
        [System.IO.StreamReader] $sr = New-Object System.IO.StreamReader -ArgumentList $context.request.InputStream;
        $postcontent = $sr.ReadToEnd();
        $message = resolveContent -path $requestedPath -context $context -handler $handler -postcontent $postcontent
    }

    [byte[]] $buffer = [System.Text.Encoding]::UTF8.GetBytes($message)
    $context.Response.ContentLength64 = $buffer.length
    $context.Response.OutputStream.Write($buffer, 0, $buffer.length)
    $context.Response.OutputStream.close()
}

function server($http){
    log "server starting loop.."
    while($context = $http.GetContext()){
        handleClient($context);
    }
}

function main {
    $endpointlocation = "http://localhost:8080/";
    $http = setup($endpointlocation);
    log "listening on $endpointlocation!"
    server($http);
    $http.stop();
}

main